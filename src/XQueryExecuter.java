import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQExpression;
import javax.xml.xquery.XQItemType;
import javax.xml.xquery.XQResultSequence;

import net.xqj.basex.BaseXXQDataSource;


public class XQueryExecuter {

	
	public XQueryExecuter () {
		
	}
	
	public String executeXQuery(String query) throws XQException {
		String queryResult = "";
		//1. open connection
		XQDataSource ds = new BaseXXQDataSource(); 
		ds.setProperty("serverName", "localhost");
		ds.setProperty("port", "1984");
		ds.setProperty("user", "admin");
		ds.setProperty("password", "admin");
		XQConnection basex = ds.getConnection();
		//2. create and execute XQuery
		XQExpression xquery = basex.createExpression();
		XQResultSequence results = xquery.executeQuery(query); 
		//3. analyse result
		while(results.next()) {
			String type = results.getItemAsString(null);
			queryResult += type + "\n";
		}
		//4. close connection
		basex.close();
		
		return queryResult;
	}
	

}

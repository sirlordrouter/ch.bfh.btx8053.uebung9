import javax.xml.xquery.XQException;


public class XQueryTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		XQueryExecuter ex = new XQueryExecuter();
		
		try {
			String result = ex.executeXQuery(
			"for $country in db:open(\"factbook\",\"factbook.xml\")//country,"+
			"$city in $country//city " +
			"where $city/population/text() > 500000 " +
			"order by number($city/population) descending " +
			"return string-join(" +
			"($country/name/text(), $city/name/text(), $city/population/text()), \"-\")"
					);
			System.out.println(result);
		} catch (XQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
